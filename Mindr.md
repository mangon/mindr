Mindr
=====

Overview
--------
In short, **DSCLAM**: a **D**istributed control of robots that does _simultaneous classification, localization and mapping_ (**SCLAM**)
 
Both Simultaneous Localization and Mapping (SLAM) and Distributed-SLAM are well-studied problems, with sound theoretical background, a large set of mature tools and many on-going applications.  The addition of "map-zone classification" might (i'm not sure) bring some novelty, although it is an obvious task.

By adding Zone Classification to Distributed-SLAM we hope to increase the performance of the SLAM team.

Progress
--------

* Layer 0: working; soft-tested; hard-tested
* Layer 1:
    * TRANSLATOR: from unicycle ($$$v,\omega$$$) to differential drive ($$$\alpha_L,\alpha_R$$$)
    * ESTIMATOR:
* Layer 2: PID implemented; soft-tested
* Layer 3: not started
Layers
------

* Level 0: hardware; direct motor control
    * ROBOT-CONTROLER (in: motor commands; out: - - -)
    * ROBOT-LISTENER (in: - - -; out: sensor readings)
* Level 1: physical representations
    * TRANSLATOR (in: command; out: motor commands) 
    * ESTIMATOR (in: sensor readings, command; out: state)
* Level 2: model-less control
    * PID (in: state, reference; out: command)
* Level 3: artificial intelligence
    * DELIBERATION (in: state; out reference)
 
SLAM
----

This is a well studied problem in robotics [1,2]. Quoting [2]:

>SLAM is the abbreviation for Simultaneous Localization And Mapping.   
>Mapping is the problem of integrating the information gathered with the robot's sensors into a given representation. It can be described by the question "What does the world look like?" Central aspects in mapping are the representation of the environment and the interpretation of sensor data. In contrast to this, localization is the problem of estimating the pose of the robot relative to a map. In other words, the robot has to answer the question, "Where am I?" Typically, one distinguishes between pose tracking, where the initial pose of the vehicle is known, and global localization, in which no a priori knowledge about the starting position is given.  
> Simultaneous localization and mapping (SLAM) is therefore defined as the problem of building a map while at the same time localizing the robot within that map. In practice, these two problems cannot be solved independently of each other. Before a robot can answer the question of what the environment looks like given a set of observations, it needs to know from which locations these observations have been made. At the same time, it is hard to estimate the current position of a vehicle without a map. Therefore, SLAM is often referred to as a chicken and egg problem: A good map is needed for localization while an accurate pose estimate is needed to build a map.

Distributed SLAM
----------------

A variation of SLAM, where a team of robots is used, instead of a single robot.  
Again, this is a well studied problem, not as much as "pure" SLAM but, nevertheless, with many easy to find publications [in google](https://www.google.com/search?q=distributed+slam&aq=f&oq=distributed+slam).

Feature Detection and Classification
------------------------------------

Besides figuring out the map layout and the robot's locations, the team must detect and classify features in the map.

Bibliography
------------

1. [wikipedia on SLAM](http://en.wikipedia.org/wiki/Simultaneous_localization_and_mapping); retrieved 25 de Abril de 2013
2. [openslam.org](http://www.openslam.org/); retrieved 25 de Abril de 2013
3. [wikipedia on Feature Detection]("http://en.wikipedia.org/wiki/Feature_detection_(computer_vision)" ); retrieved 25 de Abril de 2013;
4. [Voila, Paul, _Complex Feature Recognition: A Bayesian Approach for Learning to Recognize Objects_](http://research.microsoft.com/en-us/um/people/viola/Pubs/Detect/complexFeatureMemo.pdf); retrieved 25 de Abril de 2013;