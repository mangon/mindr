package tracking

type PID struct {
	p, i, d float64
	reference, previous, accumulated, current float64
	age int
}

func New(p, i, d, r float64) PID {
	n := PID{}
	n.p = p
	n.i = i
	n.d = d
	n.reference = r

	return n
}

func (t *PID) Track(x float64) float64 {
	t.current = t.reference  - x
	t.age += 1
	t.previous = t.current
	t.accumulated += t.current
	return t.p * t.current + t.i * t.accumulated + t.d * t.Variation()

}

func (t PID) Current() float64 {
	return t.current
}

func (t PID) Previous() float64 {
	return t.previous
}

func (t PID) Integral() float64 {
	return t.accumulated
}

func (t PID) Variation() float64 {
	return t.current - t.previous
}

func (t PID) Reference() float64 {
	return t.reference
}

func (t PID) Parameters() (float64, float64, float64) {
	return t.p, t.i, t.d
}
