#Tech Notes

There seems to be a convergence towards the use of JVM in this project:

* At the lowest level, the communications with the NXT, the only library I can get to work (in my mac) is leJOS, a java library;
* At the top level, it seems that our work will be based in jason (a java library);

So I searched for numerical (linalg) libraries in java and found a few, of which I selected (for no special reason) colt;

It would be nice if this framework could be used from languages like python, groovy or scala. The major obstacle is that the leJOS library requires a special taste of java…
