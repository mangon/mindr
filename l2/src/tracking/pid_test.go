package tracking

import (
	"testing";
	"math";
	"fmt";
)

func Test_PID_1(t *testing.T) {

	r := 0.7
	delta := 0.001


	p := New(0.5, 1.0, 0.01, r)

	x := -7.2
	n := 0
	u := 0.0
	for math.Abs(x-r) > delta {
		//t := float64(i) * (5.0 / float64(N))
		u = p.Track(x)
		n++
		fmt.Printf("n: %3d\tx: %7.3f\tu: %7.3f\n",n,x,u)
		x += u
	}
}
