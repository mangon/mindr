#!/usr/bin/python
import socket
import time
import random

def average(a):
	n = len(a)
	return reduce(lambda x,y: x+y, a)/float(n)

HOST = '127.0.0.1'    # The remote host
PORT = 33310              # The same port as used by the server

def controlRobot(sock, cmd):

	sock.send(cmd + '\n')
	data = sock.recv(128)
	percept = data

	while data[-1] != "\n" :
		data = sock.recv(128)
		percept += data
	return percept[:-1]

def testControlRobot():
	commands = [ '0.0;0.0', '0.0;0.0', '0.0;0.0', '0.0;0.0', '0.0;0.0' ]

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		s.connect((HOST, PORT))
	except Exception as e:
		print e
		return None

	i = 0
	lat = list()
	while True:
		t1 = time.time()
		cmd = ''
		if i % 2 == 0:
			at = 0.1
			bt = 0.5
			ct = 1.0

			cmd = '%3.1f;%3.1f;%3.1f'%(at,bt,ct)
		else:
			cmd = '0.0;0.0;0.0'

		r = controlRobot(s, cmd)
		if r is None:
			break

		i += 1
		t2 = time.time()
		lat = lat + [1000*(t2-t1)]
		print "-----\nsend:\t%s\ngot:\t%s\ntook:\t%0.3fms"%(cmd,r,1000*(t2-t1))
		print average(lat)
	s.close()

if __name__ == "__main__":
	testControlRobot()

