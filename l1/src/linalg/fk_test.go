package linalg

import "fmt"
import "testing"


func ExampleMatrix() {
	A := Matrix{ []float64{1, 2, 3, 4}, 2, 2 }

	for r := 0; r < 2; r++ {
		for c := 0; c < 2; c++ {
			fmt.Printf("[%3.1f]",A.At(r,c))
		}
		fmt.Println()
	}

	// Output:
	// [1.0][2.0] 
	// [3.0][4.0] 
}

func TestCholesky(t *testing.T) {
	A := Matrix{ []float64{3, 4, 1, 4, 3, 4, 0, 1, 1}, 3, 3}
	B := A.Mul( A.Transpose() )

	L := B.Cholesky()
	C := L.Mul( L.Transpose() )

	if !B.Equal(C, 1E-5) {
		t.Errorf("Failed Cholesky")
	}
}

/*
import "testing"

func stateUpdate(A,B,x,u []float64, an, am, bm int) []float64 {
	return Plus( Mul(A,x, an, am, 1), Mul(B,u, an, bm, 1))
}

func Test_FK_Constant(t *testing.T) {
	A := []float64{1.0}
	B := []float64{0.0}
	H := []float64{1.0}


}
*/
