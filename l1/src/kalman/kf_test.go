package kalman

import . "linalg"
import "math/rand"
import "math"
import "fmt" // for example output
import "io/ioutil" // to write files

// Apply the KalmanFilter to find a constant value
func ExampleConstant() {
	// true value, to be found
	true_x := 22.12345
	// number of steps to find the true value
	numSteps := 50

	// transition: identity (true_x is *constant*)
	A := Eye(1)
	// control model: no effect
	B := Zeros(1,1)
	// observation model: full observable
	H := Eye(1)

	// transition noise covariance
	q := 1E-5
	Q := Diag( []float64{ q } )

	// observation noise covariance
	r := 0.1
	R := Diag( []float64{ r } )

	//
	//	Preparation of
	//		Control and Observation sequences 
	//	
	U := make([]Matrix, numSteps)
	Z := make([]Matrix, numSteps)

	rand.Seed( 31415 )
	// at each step
	for k := 0; k < numSteps; k++ {
		// no control signal
		U[k] = Zeros(1,1)
		// noisy observation
		Z[k] = Eye(1).Scale( true_x + rand.NormFloat64() * r )
	}

	// initial estimation (equals the first observed value)
	x0 := Zeros(1,1).Plus(Z[0])
	// initial covariance
	P0 := Eye(1).Scale( 0.1 )

	//
	//	Process the Kalman Filter
	//
	xhat := ProcessKalmanFilter(A,B,Q, H,R, x0, P0, Z,U)

	estimationError := math.Abs(xhat[ numSteps -1 ].At(0,0) - true_x)

	if estimationError < 1E-2 {
		fmt.Println("small(ish) error.")
	} else {
		fmt.Printf("big error: %7.5f. bad bad bad.",estimationError)
	}
	// Output:
	// small(ish) error.
}


func Example1DPointMass_1() {
	numSteps := 100
	dt := 15.0 / float64(numSteps)
	// transition
	A := Eye(2).Plus(FromSlice( []float64{0, 1, 0, 0}, 2, 2).Scale( dt ))
	// control
	B := FromSlice( []float64{0, 1}, 2, 1)
	// measure
	H := FromSlice( []float64{1, 0}, 1, 2)
	// process noise covariance
	q11, q22 := 1E-4, 1E-4
	Q := Diag( []float64{q11, q22} )
	w := CovarNoise(Q)
	// observation covariance
	r := 5E-2
	R := Diag( []float64{ r } )
	v := CovarNoise(R)
	// initial state
	x0 := FromSlice( []float64{1,0.01}, 2, 1)
	// Build a random trajectory
	U := make([]Matrix, numSteps)
	Z := make([]Matrix, numSteps)
	X := make([]Matrix, numSteps)
	x := x0.Clone()
	for k := 0; k < numSteps; k++ {
		u := 0.0
		if k < numSteps / 4 {
			u = 0.01
		} else if 4*k <  3*numSteps {
			u = -0.01
		} else {
			u = 0.0
		}
		U[k] = Eye(1).Scale(u)
		x = UpdateState(A,B,x, U[k]).Plus( w() )
		X[k] = x
		Z[k] = Measure(H, x).Plus( v() )
	}

	// initial guesses covar
	d := 0.5
	P0guess := Ones(2,2).Scale(d).Plus( Eye(2).Scale(1-d) )
	x0guess := Zeros(2,1)
	Xhat := ProcessKalmanFilter(A,B, Q, H, R, x0guess, P0guess, Z, U)

	s := "time,real,measured,predicted\n"
	for i,p := range X {
		s += fmt.Sprintf("%f,%f,%f,%f \n",float64(i)*dt,p.At(0,0),Z[i].At(0,0), Xhat[i].At(0,0))
	}

	ioutil.WriteFile("output.dat", []byte(s), 0644)
	// Output:

}
