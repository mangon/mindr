package linalg

import "testing"

func Test_Mul(t *testing.T) {
	a := Matrix{[]float64{2.0, 3.0, 4.0, 5.0, 6.0, 7.0}, 3, 2}
	b := Matrix{[]float64{4.0, 3.0, 2.0, 1.0}, 2, 2}

	c := a.Mul(b)
	d := Matrix{[]float64{14.0, 9.0, 26.0, 17.0, 38.0, 25.0}, 3, 2}
	if !d.Equal(c, 1E-5) {
		t.Errorf("multiplication failed")
	}
}

func Test_GaussJordan_1(t *testing.T) {
	a := Matrix{[]float64{2.0, 0.0, 0.0, 3.0}, 2, 2}
	b := Matrix{[]float64{1.0, 1.0}, 2, 1}

	d, x := GaussJordan(a, b)

	if (x.Pos(0) != 0.5) || (x.Pos(1) != 1.0/3.0) {
		t.Errorf("det: %f\nsol: %v", d, x)
	}

}

func Test_GaussJordan_2(t *testing.T) {
	a := Matrix{[]float64{1, 2, 3, 4, 5, 6, 7, 9, 9}, 3, 3}
	b := Matrix{[]float64{2, 3, 4, 5, 6, 7, 8, 9, 1}, 3, 3}

	ab := a.Mul(b)

	d, x := GaussJordan(a, ab)

	ax := a.Mul(x)
	if !ax.Equal(ab, 1e-5) {
		t.Errorf("a = %v\nb = %v\nab = %v\nd = %v\nx = %v\nax = %v", a, b, ab, d, x, ax)
	}
}

func Test_Inverse(t *testing.T) {
	a := Matrix{[]float64{1, 2, 3, 4, 5, 6, 7, 9, 9}, 3, 3}
	ai := a.Inverse()
	r := Matrix{[]float64{-1.5, 1.5, -0.5,1.0,-2.0,1.0,0.16667,0.83333,-0.5}, 3, 3}

	if !ai.Equal(r, 1E-5) {
		t.Errorf("inverse: %v",ai)
	}
}

func Test_Solve(t *testing.T) {
	a := Matrix{[]float64{1, 2, 3, 4, 5, 6, 7, 9, 9}, 3, 3}
	b := Matrix{[]float64{2, 3, 4, 5, 6, 7, 8, 9, 1}, 3, 3}

	r := Matrix{[]float64{0.5, 0, 4, 0, 0, -9, 0.5, 1, 6}, 3, 3}

	x := a.SolveTo(b)

	if !r.Equal(x, 1E-5) {
		t.Errorf("x: %v",x)
	}
}

