package conv

import "math"

const Ti float64 = 2.0 * math.Pi
const Ip float64 = 1.0 / math.Pi
const I180 float64 = 1.0 / 180.0

// Function Unicycle_2_DiffDriveLV converts a unicycle command (v,w) where
// - v is the linear velocity and
// - w is tha angular velocity
// into a differential drive command (vl,vr) where
// - vl is the linear velocity of the left wheel and
// - vr is the linear velocity of the right wheel
// This conversion has additional parameters:
// - L is the distance between the center of the wheels
// - R is the wheel's radius
func Unicycle_2_DiffDriveLV(v,w float64, L,R float64) (vl, vr float64) {
	c := 0.5 / R
	u := 2 * v
	a := w * L
	vr = c * (u + a)
	vl = c * (u - a)
	return
}

// Function DiffDriveLV_2_DiffDriveAV converts linear velocity differential drive into angular motion differential drive
func DiffDriveLV_2_DiffDriveAV(v, R float64) float64 {
	return v / Ti * R
}

func Tachometer_2_WheelDistance(tach float64, R, N float64) (dist float64) {
	dist = Ti * R * tach / N
	return
}

func WheelsDistance_2_DeltaPose(dl, dr float64, L, theta float64) (dx, dy, dt float64) {
	dc := 0.5 * (dl + dr)
	st,ct := math.Sincos(theta)
	dx = dc * ct
	dy = dc * st
	dt = (dr - dl) / L
	return
}

//
//
//	TRIGONOMETY
//
//
func AngleTo(dx,dy float64) float64 {
	return math.Atan2(dy,dx)
}

func Deg(rad float64) float64 {
	return 180.0 * rad * Ip
}

func Rad(deg float64) float64 {
	return math.Pi * deg * I180
}

