import lejos.nxt.SensorPort;

import lejos.nxt.UltrasonicSensor;
import lejos.nxt.SoundSensor;
import lejos.nxt.LightSensor;
import lejos.nxt.TouchSensor;

import lejos.nxt.SensorConstants;
import lejos.nxt.I2CPort;

import lejos.robotics.navigation.DifferentialPilot;

import lejos.nxt.Motor;
import lejos.nxt.Sound;
import lejos.nxt.Battery;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommLogListener;
import lejos.pc.comm.NXTCommandConnector;
import lejos.pc.comm.NXTConnector;
import lejos.nxt.remote.NXTCommand;

import java.io.IOException;
import java.math.*;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import jason.*;
//import java.lang.Thread;

public class NXTProxy implements Runnable {
	//
	//
	//	NXT CONNECTION CODE
	//
	//
    private NXTConnector brick;
	private TouchSensor touch;
    private SoundSensor sound;
	private LightSensor light;
	private UltrasonicSensor distance;
	/**
	 *	connectNXT tries to establish a bluetooth connection with brick named "name";
	 *	if connection is successful, the method "onConnect" is evocated;
	 */
	public void connectNXT(String name) {
        brick = new NXTConnector(); // this "brick" holds the connection details, right?
		/*
		brick.addLogListener(new NXTCommLogListener() {
            public void logEvent(String message) {
                System.out.println(message);				
            }

            public void logEvent(Throwable throwable) {
                System.err.println(throwable.getMessage());			
            }			
        });
		*/
        //brick.setDebug(true);

		if (!brick.connectTo("btspp://"+name, NXTComm.LCP)) {
            System.err.println("Failed to connect");
            System.exit(1);
        } else {
		   System.out.printf("Connected to %s\n",name);
		}	   
		
		NXTCommandConnector.setNXTCommand(new NXTCommand(brick.getNXTComm()));

		onConnect();
    }

	/**
	 * onConnect runs after a successful connection with a brick;
	 * this method is used to setup sensors and motors;
	 */
	private void onConnect() {
		//
		// Instantiate sensors
		//
		touch = new TouchSensor(SensorPort.S1);
		sound = new SoundSensor(SensorPort.S2);
		light = new LightSensor(SensorPort.S3);
		distance = new UltrasonicSensor(SensorPort.S4);
		SensorPort.S1.setType(I2CPort.TYPE_HISPEED);
		SensorPort.S2.setType(I2CPort.TYPE_HISPEED);
		SensorPort.S3.setType(I2CPort.TYPE_HISPEED);
		SensorPort.S4.setType(I2CPort.TYPE_HISPEED);

		//
		// Calibrate motors
        //
		Motor.C.setSpeed(360);
		Motor.C.flt(true);
        
		Motor.B.setSpeed(360);
		Motor.B.flt(true);
		
		Motor.A.setSpeed(360);
        // Motor.A.flt(true);

	}

    public void closeNXT() {
        try {
            brick.close();
        } catch (IOException e) {
            System.err.println("Failed closeNXT");
			System.err.println(e);
        }
    }


	/**
	 * move sends move commands to the brick
	 */
    private void moveNXT(double aTurns, double bTurns, double cTurns) {
 
		int aDeg = (int) (aTurns * 360);
		int leftDeg = (int) (bTurns * 360);
        int rightDeg = (int) (cTurns * 360);

        Motor.C.rotate(leftDeg,true);
        Motor.B.rotate(rightDeg,true);
        
		//Motor.C.stop();
        //Motor.B.stop();

		Motor.A.rotate(aDeg, true);
    }

	/**
	 * sense gets sensors readings from the brick
	 * 
	   Index	|	Port	|	Default Sensor
	   ---------|-----------|------------------
	   0		|	S1		|	TOUCH
	   1		|	S2		| 	SOUND
	   2 		| 	S3 		| 	AMBIENT LIGHT
	   3 		| 	S3 		| 	REFLECTED LIGTH
	   4 		| 	S4 		| 	ULTRASONIC
	   5 		| 	Battery	| 	BATTERY
	   6 		| 	A 		| 	MOTOR TACHO A
	   7 		| 	B 		| 	MOTOR TACHO B
	   8 		| 	C 		| 	MOTOR TACHO C

	 */
    private int[] senseNXT() {
        int[] s = new int[9];

        s[0] = touch.isPressed() ? 1 : 0;
        
        s[1] = sound.readValue();

        light.setFloodlight( false );
        s[2] = light.readValue();
        
		light.setFloodlight( true );
		s[3] = light.readValue();

        distance.ping();
        s[4] = distance.getDistance();

        s[5] = Battery.getVoltageMilliVolt();

		s[6] = Motor.A.getTachoCount();
		s[7] = Motor.B.getTachoCount();
		s[8] = Motor.C.getTachoCount();

        return s;
    }

	//
	//
	//	SOCKET SERVER CODE
	//
	//
	private ServerSocket server;
	private Socket socket;
	private BufferedReader motorReader;
	private DataOutputStream sensorWriter;

	public void run() {
		int[] senseVector = new int[5];
		double[] motorVector = new double[2];
		String motorLine, sensorLine;
		String[] arguments;
		Double aAngle, bAngle, cAngle;

		try {
			while ((motorLine = motorReader.readLine()) != null) {

				// sense and send sensor values
				long beforeSense = System.currentTimeMillis();
				senseVector = senseNXT();
				long afterSense = System.currentTimeMillis();
				System.out.printf("Time to sense: %d\n",afterSense - beforeSense);

				sensorLine = String.format("%d;%d;%d;%d;%d;%d;%d;%d;%d\n",
						senseVector[0],
						senseVector[1],
						senseVector[2],
						senseVector[3],
						senseVector[4],
						senseVector[5],
						senseVector[6],
						senseVector[7],
						senseVector[8]  );

				sensorWriter.writeBytes( sensorLine );
				sensorWriter.flush();

				// read and execute motor commands
				//
				// Index	Motor
				// 0		A
				// 1		B
				// 2		C
				arguments = motorLine.split(";");

				aAngle = Double.valueOf(arguments[0]);
				bAngle = Double.valueOf(arguments[1]);
				cAngle = Double.valueOf(arguments[2]);

				long beforeMove = System.currentTimeMillis();
				moveNXT(aAngle, bAngle, cAngle);
				long afterMove = System.currentTimeMillis();
				System.out.printf("Time to move: %d\n",afterMove - beforeMove);
			}

			motorReader.close();
			sensorWriter.close();
			socket.close();
			server.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * serveSocket installs a socket server in port "port"
	 * and launches a control loop in a new thread.
	 */
    public void serveSocket(int port) throws Exception {

        server = new ServerSocket(port);
        System.out.printf("Serving in port %d\n",port);
        
		socket = server.accept();
		socket.setReceiveBufferSize(128);
		socket.setSendBufferSize(128);
		motorReader = new BufferedReader( new InputStreamReader(
					socket.getInputStream() ));

		sensorWriter = new DataOutputStream( socket.getOutputStream());
        
		new Thread(this).start();
    }

    public static void main(String[] args) throws Exception {
		System.out.printf("Jason library imported");
		NXTProxy p = new NXTProxy();
		p.connectNXT("SAGAN");
        p.serveSocket(33310);        
    }
}
