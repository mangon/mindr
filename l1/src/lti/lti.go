package lti

import . "linalg"

type DynamicSystem interface {
	Evolve(x,u *Matrix) Matrix
	Measure(x *Matrix) Matrix
}

type LTI struct {
	A, B, C Matrix
}

func (s LTI) Evolve(x, u *Matrix) Matrix {
	return  s.A.Mul(x).Plus( s.B.Mul(u) )
}

func (s LTI) Measure(x *Matrix) Matrix {
	return s.C.Mul(x)
}


