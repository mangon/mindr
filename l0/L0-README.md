# NXT Proxy

## Usage

1. run `. ./env-leJOS` (yes, **DOT SPACE DOT SLASH env-leJOS**) to define a number of environment variables
2. run `ant`
3. the expected output is displayed below. The last line might take up to a minute to print.  
    Buildfile: <$CWD>/build.xml  

    clean:  
        [delete] Deleting directory <$CWD>/build  
    
    compile:  
        [mkdir] Created dir: <$CWD>/build  
        [javac] Compiling 1 source file to <$CWD>/build  

    run:  
        [java] BlueCove version 2.1.0 on mac  
        [java] Serving SAGAN proxy in port 33310


## API

---
* **port** 33310
* **robot** SAGAN

---
**Sense Vector**

Index | Port | Default Sensor
---|---|---:
0 | S1 | TOUCH
1 | S2 | SOUND
2 | S3 | REFLECTED LIGHT
3 | S3 | AMBIENT LIGTH
4 | S4 | ULTRASONIC
5 | Battery	| BATTERY
6 | A | MOTOR TACHO A
7 | B | MOTOR TACHO B
8 | C | MOTOR TACHO C

---	

**Motor Vector**

Index | Motor Port | Default Assignment
---|---|---:
0 | A | Action
1 | B | Left Wheel
2 | C | Right Wheel

---

## Caveats

* Current version of [bluecove](https://code.google.com/p/bluecove/), the common java implementation of the bluetooth stack requires 32 bits libraries in OS X. That is unlikely to change, since the bluecove development seems stalled;

* Java 1.7 doesn't support 32 bits libraries from previous versions (**a perfect storm**). The workaround is to use java version 1.6;