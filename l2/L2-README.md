#Level 2: Model - Proxy

**Abstract:** The level below this (proxy) understands direct commands to motors and direct readings from sensors. Above this level is defined the general behavior, that builds plans, uses beliefs and setup waypoints. Here is implemented a set of abstractions and approximations to support higher level control.

At the highest level we have a `behavior` that receives a **state estimation**, $$$\hat{x}$$$, and produces **references**. Below it there are two modules: `estimators` that use **perceptions**, $$$p$$$,  to update the state estimation and the `trackers` that produce **controls**, $$$u$$$ using the state estimation and the references. The lowest level abstracts the hardware using `sensors` that scan the environment to produce perceptions and `actuators` that affect the environment according to the controls.

**Keywords:** `behavior`, `estimator`, `tracker`, `sensor`, `actuator`, **perception** ($$$p$$$), **estimation** ($$$\hat{x}$$$), **reference** ($$$r$$$), **control** ($$$u$$$).

|   | `b` | `e` | `t` | `a` | `s` | |
|---|---|---|---|---|---|-:
| **`b`** |   |   | $$$r$$$ |   |   |`behaviour`
| **`e`** |$$$\hat{x}$$$ |   |$$$\hat{x}$$$ |   |   |`estimators`
| **`t`** |   |   |   | $$$u$$$ |   |`trackers`
| **`a`** |   |   |   |   |   |`actuators`
| **`s`** |   | $$$p$$$|   |   |   |`sensors`
 
##Work Plan

1. Define, in paper, a convenient set of abstract tools: models, functions, data-structures...
2. Select, from that set, the ones required to make things work
3. Implement selected tools

##Models and Abstractions

* Basic calculations
    * Solving linear systems $$$AX = B$$$
    * Computing eigenvalues and eigenvectors (zeros of polynomials)
    * Pole placement: find $$$X$$$ such that $$$\textrm{eig}(X) = \lambda_1, \lambda_2, \ldots, \lambda_n$$$ 
* Robot state representation:
    * [Robot in the floor](#POSE)
* Nearby Obstacles:
    * [Sensor Disk](#SENSORDISK)
* Motion models:
    * [Unicycle](#UNICYCLE)
    * [Differential Drive](#DIFFERENTIALDRIVE)
* Sensors:
    * [Tachometer](#TACHOMETER)
* Actuators:
    * [Tachometer](#TACHOMETER)         

### [Track](id:TRACK)
The most used (almost) **stable, model-agnostic and robust tracking** function is the `PID` controller. The assumption is that given a **state**, $$$x_t$$$, and a **reference**, $$$r$$$, we want to compute a **control**, $$$u_t$$$, to drive the state to the reference, _i.e._ to zero the **error**, $$$e_t:=r-x_t$$$. We can do
$$
u_t = PID(e_t) = K_Pe_t+ K_I \int_{t_0}^t e\_{\tau} d\tau  + K_D \dot{e}\_t
$$
parameterized by the constants $$$K_P, K_I, K_D$$$.

 
### [Pose](id:POSE)
A common representation of a (floor) robot:  
$$
\mathrm{Pose} = \left( x,y,\theta \right)
$$

### [Sensor Disk](id:SENSORDISK)
This abstraction assumes that 

1. Obstacles are points;
2. Within a certain radius, $$$d^\top$$$, distance and direction to obstacles are known:

$$
\mathrm{Obstacles} = \left< \left(d_i, \theta_i\right)\middle| i = 1,\ldots, N  \right>
$$
If the pose $$$\left(x,y,\theta\right)$$$ is known then the position of each obstacle can be found:

\\[
\left\\{ \begin{array}{rcl}
x_i &=& x + d_i \cos( \theta + \theta_i)\\\
y_i &=& y + d_i \sin( \theta + \theta_i)
\end{array}\right.
\\]

### [Unicycle](id:UNICYCLE)
A simple representation of the control of a mobile robot, where the robot has no extension, only a pose that is updated by linear and angular velocities, resp. $$$v,\omega$$$:

\\[
\left\\{ \begin{array}{rcl}
\dot{x} &=& v\cos( \theta )\\\
\dot{y} &=& v\sin( \theta )\\\
\dot{\theta} &=& \omega
\end{array}\right.
\\]

### [Differential Drive](id:DIFFERENTIALDRIVE)
A robot with two wheels side-by-side. Parameters are  

*  wheel radius, $$$R$$$, and
*  wheel distance, $$$L$$$.

The pose is updated setting the (linear) velocities of each wheel, $$$v_l, v_r$$$:

\\[
\left\\{ \begin{array}{rcl}
\dot{x} &=& R\frac{v_r + v_l}{2}\cos \theta\\\
\dot{y} &=& R\frac{v_r + v_l}{2}\sin \theta\\\
\dot{\theta} &=&  R\frac{v_r - v_l}{L}
\end{array}\right.
\\]

It is possible to convert a unicycle control, $$$v,\omega$$$, to a differential drive control, $$$v_l,v_r$$$:

\\[
\left[ \begin{array}{c} v_r \\\ v_l \end{array} \right] = \frac{1}{2R} \left[ \begin{array}{cc} 2 & L \\\ 2 & -L \end{array} \right]\left[ \begin{array}{c} v \\\ \omega \end{array} \right]
\\]

and reciprocally

\\[
\left[ \begin{array}{c} v \\\ \omega \end{array} \right] = \frac{R}{2L} \left[ \begin{array}{cc} L & L \\\ 2 & -2 \end{array} \right]\left[ \begin{array}{c} v_r \\\ v_l \end{array} \right]
\\]

### [Tachometer control/sensor](id:TACHOMETER)
Assuming the differential drive model, and that:  

1. Each wheel has $$$N$$$ ticks per revolution ($$$2\pi$$$)
2. Each wheel is equipped with a tachometer that reports the total number of "ticks", $$$T_r,T_l$$$

Then, each wheel covers a distance

$$
D_s'  = 2\pi R\frac{T_s' - T_s}{N}
$$

where $$$s = r,l$$$. Also

$$
D_c = \frac{D_r + D_l}{2}
$$

and 

\\[
\left\\{ \begin{array}{rcl}
x' &=& x + D_c \cos \theta\\\
y' &=& y + D_c\sin \theta\\\
\theta' &=&  \theta + \frac{D_r - D_l}{L}
\end{array}\right.
\\]

The conversion of $$$D_r, D_l$$$ to $$$v_r,v_l$$$ is straightforward

\\[
\left[ \begin{array}{c} v_r \\\ v_l \end{array} \right] = \frac{1}{\delta_t} 
\left[ \begin{array}{c} D_r \\\ D_l \end{array} \right]
\\]

and reciprocally


\\[
\left[ \begin{array}{c} D_r \\\ D_l \end{array} \right] =  \delta_t\left[ \begin{array}{c} v_r \\\ v_l \end{array} \right]
\\]