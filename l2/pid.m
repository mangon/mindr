function x = pid(Kp, Ki, Kd, r, x0)
	n = length(r);
	x = repmat(0, 1, n);
	x(1) = x0;
	E = 0;
	de = 0;
	for i = 2:n
		e = r(i) - x(i-1);
		D = e - de;
		E = e + E;
		u = Kp * e + Ki * E + Kd * D;
		x(i) = x(i-1) + u;
		de = e;
	end
	return
end




