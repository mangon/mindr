package linalg

import "math" // for Abs and Sqrt

type Matrix struct {
	d    []float64
	n, m int
}

func (a Matrix) Dim() (int, int) {
	return a.n, a.m
}

/**
Matrix entry access
*/

func (a Matrix) At(i, j int) float64 {
	return a.d[a.Index(i, j)]
}

func (a Matrix) Pos(c int) float64 {
	return a.d[c]
}

func (a Matrix) SetAt(i, j int, x float64) {
	a.d[a.Index(i, j)] = x
}

func (a Matrix) SetPos(c int, x float64) {
	a.d[c] = x
}

func (a Matrix) Slice(i, j, r,c int) Matrix {
	b := Zeros(r,c)
	for x := 0; x < r; x++ {
		for y := 0; y < c; y++ {
			z := a.At(i+x, j+y)
			b.SetAt(x,y, z)
		}
	}

	return b
}

func (a Matrix) Place(i,j int, b Matrix) Matrix {
	c := a.Clone()
	for x := 0; x < b.n; x++ {
		for y := 0; y < b.m; y++ {
			z := b.At(x,y)
			c.SetAt(i+x,j+y, z )
		}
	}

	return c
}

// index computes the array-index of entry (i,j) in a n x m matrix
func (a Matrix) Index(i, j int) (c int) {
	if (0 <= i) && (i < a.n) && (0 <= j) && (j < a.m) {
		c = i*a.m + j
	} else {
		c = -1
	}
	return
}

// coord computes the n x m matrix-position (i,j) of the array-index c
func (a Matrix) Coord(c int) (i, j int) {
	if (0 <= c) && (c < a.n*a.m) {
		i = c / a.m
		j = c % a.m
	} else {
		i = -1
		j = -1
	}
	return
}

// swapLines does the Li <-> Lj matrix lines operation
func (a Matrix) swapLines(i, j int) {
	if a.m == 1 {
		a.d[i], a.d[j] = a.d[j], a.d[i]
	} else {
		for k := 0; k < a.n; k++ {
			u := a.Index(i, k)
			v := a.Index(j, k)
			a.d[u], a.d[v] = a.d[v], a.d[u]
		}
	}

	return
}

// multLine does the Li -> alpha Li matrix lines operation
func (a Matrix) multLine(i int, alpha float64) {
	if a.m == 1 {
		a.d[i] *= alpha
	} else {
		for k := 0; k < a.n; k++ {
			u := a.Index(i, k)
			a.d[u] *= alpha
		}
	}
	return
}

// combLines does the Lj -> Lj + alpha Li matrix line operation
func (a Matrix) combLines(i, j int, alpha float64) {
	if a.m == 1 {
		a.d[j] += alpha * a.d[i]
	} else {
		for k := 0; k < a.n; k++ {
			u := a.Index(i, k)
			v := a.Index(j, k)
			a.d[v] += alpha * a.d[u]
		}
	}
	return
}

// Equal tests if |a - b| < tol
func (a Matrix) Equal(b Matrix, tol float64) bool {
	// check dimensions
	bn, bm := b.Dim()
	if a.n != bn || a.m != bm {
		return false
	}
	// check entries
	for i, x := range a.d {
		if math.Abs(x-b.Pos(i)) >= tol {
			return false
		}
	}
	return true
}

// Clone ...
func (a Matrix) Clone() Matrix {
	d := make([]float64, len(a.d))
	copy(d, a.d)

	return Matrix{d, a.n, a.m}
}

// Eye returns the nxn identity matrix
func Eye(n int) Matrix {
	d := make([]float64, n*n)
	for k := 0; k < n; k++ {
		d[k*n+k] = 1.0
	}

	return Matrix{d, n, n}
}

// Zeros returns a n = ?x? zeros matrix 
func Zeros(n, m int) Matrix {
	d := make([]float64, n*m)
	return Matrix{d, n, m}
}

// Ones returns a n = ?x? ones matrix 
func Ones(n, m int) Matrix {
	nm := n * m
	d := make([]float64, nm)
	for i := 0; i < nm; i++ {
		d[i] = 1.0
	}

	return Matrix{d, n, m}
}

// Diag ...
func Diag(d []float64) Matrix {
	n := len(d)
	a := Zeros(n, n)
	for i := 0; i < n; i++ {
		a.SetAt(i, i, d[i])
	}

	return a
}

// FromSlice ...
func FromSlice(d []float64, n, m int) Matrix {
	return Matrix{ d, n, m}
}

// Scale computes the scalar product x A
func (a Matrix) Scale(x float64) Matrix {
	nm := a.n * a.m
	d := make([]float64, nm)
	for k := 0; k < nm; k++ {
		d[k] = x * a.d[k]
	}

	return Matrix{d, a.n, a.m}
}

// CanSum ...
func (a Matrix) CanSum(b Matrix) bool {
	bn, bm := b.Dim()
	return a.n == bn && a.m == bm
}

// Plus computes c = a + b
// Assumes that a and b have the same Dim
func (a Matrix) Plus(b Matrix) Matrix {
	n := len(a.d)
	d := make([]float64, n)
	for k := 0; k < n; k++ {
		d[k] = a.d[k] + b.Pos(k)
	}

	return Matrix{d, a.n, a.m}
}

// Transpose computes the transpose b_ij =  a_ji
func (a Matrix) Transpose() Matrix {
	nm := a.n * a.m
	c := Zeros(a.m, a.n)
	for k := 0; k < nm; k++ {
		i, j := a.Coord(k)
		c.SetAt(j, i, a.d[k])
	}

	return c
}

// CanMul ...
func (a Matrix) CanMul(b Matrix) bool {
	bn, _ := b.Dim()
	return a.m == bn
}

// Mul computes c = a x b
// Assumes that a and b have compatible dim
func (a Matrix) Mul(b Matrix) Matrix {
	bn, bm := b.Dim()
	c := Zeros(a.n, bm)
	for i := 0; i < a.n; i++ {
		for j := 0; j < bm; j++ {
			sum := 0.0
			for k := 0; k < bn; k++ {
				sum += a.At(i, k) * b.At(k, j)
			}
			c.SetAt(i, j, sum)
		}
	}

	return c
}

// Rank is not yet implemented
// Rank computes the number of lines/rows linearly independent in a
func (a Matrix) Rank() int {
	rank := 0
	return rank
}

// Determinant ...
func (a Matrix) Determinant() float64 {
	t := Zeros(a.n, 1)
	d,_ := GaussJordan(a, t )
	return d
}

// Inverse ...
func (a Matrix) Inverse() Matrix {
	t := Eye(a.n)
	_, i := GaussJordan(a, t)
	return i
}

// SolveTo ...
func (a Matrix) SolveTo(b Matrix) Matrix {
	_,ai := GaussJordan(a,b)
	return ai
}

/*
// Eig ...
func (a Matrix) Eig() (Matrix Matrix) {
	
}
*/

// Null is not yet implemented
// Null computes a basis of the null space of a, ie: an = 0
//func (a Matrix) Null() Matrix {
//	null := make([]float64, m)
//	null_n := m
//	null_m := 1
//
//	return (null, null_n, null_m)
//}

// CaussJordan computes the determinant of a and a solution x of ax=b
func GaussJordan(a, b Matrix) (det float64, x Matrix) {
	c := a.Clone()
	x = b.Clone()

	max_car := a.n
	if a.m < a.n {
		max_car = a.m
	}

	pivot := 1.0
	det = 1.0
	for i := 0; i < max_car; i++ {
		j := 0
		found := false
		for ; j < max_car; j++ {
			pivot = c.At(i, j)
			if pivot != 0.0 {
				found = true
				break
			}
		}
		if !found {
			det = 0.0
			return
		}
		if i != j {
			det = -det
			c.swapLines(i, j)
			x.swapLines(i, j)
		}
		ip := 1.0 / pivot
		c.multLine(i, ip)
		x.multLine(i, ip)
		det *= pivot
		for j = 0; j < max_car; j++ {
			if j != i {
				r := -c.At(j, i)
				c.combLines(i, j, r)
				x.combLines(i, j, r)
			}
		}
	}
	return
}

// IsSDPositive ...
// is not the same as symmetrical, you dumb!
// indeed:
// A >= 0 <=> forall x, xT A x >= 0
// and, it turns out that
// B = [ 1 2 ; 2 1 ] is symmetrical but not >= 0
func (a Matrix) IsPositiveSD() bool {
	return true
}


// Cholesky
//	if a is Positive SD returns L such that a = L.Mul( L.Transpose() )
//	otherwise probably crashes your computer
func (a Matrix) Cholesky() Matrix {
	n := a.n
	Ai := a.Clone()
	L := Eye(n)
	for i := 0; i < n; i++ {
		j := n - i // INVARIANT: n == i + j 
		//			I	0	0	
		// aux = 	0	x	u	i-th row
		//			0	v	b
		x := Ai.At(i,i)
		u := Ai.Slice( i,i+1 , 1,j-1 )
		v := Ai.Slice( i+1,i , j-1,1 )
		b := Ai.Slice( i+1,i+1, j-1,j-1 )

		// update Ai 
		Ai = Eye(n).Place(i+1,i+1, b.Plus( v.Mul(u).Scale(-1.0/x) ) )
		// compute Li
		y := math.Sqrt(x)
		Li := Eye(n)
		Li.SetAt(i,i, y )
		v1 := v.Scale(1.0 / y )
		Li = Li.Place(i+1,i, v1 )
		// update L
		L = L.Mul(Li)
	}
	return L
}

/*
// Place...
// computes a matrix K such that eig(A - BK) == P
//
func Place(A,B,P Matrix) Matrix {
}
*/





