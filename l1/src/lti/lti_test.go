package lti

import . "linalg"
import "testing"

func Test_LTI_1(t *testing.T) {
	system := LTI {
		A: FromSlice([]float64{
			0.1, 0.0, 1.0,
			1.0, 0.1, 0.0,
			0.0, 1.0, 0.1 },3,3), 
		B: Ones(3,2),
		C: Ones(1,3),
	}

	x := FromSlice( []float64{ 0.0, 1.0, 2.0 }, 3, 1)
	u := FromSlice( []float64{ 1.0, 0.1}, 2,1)
	x = system.Evolve(&x,&u)
	y := system.Measure(&x)

	t.Errorf("**JUST TO SEE SOMETHING PRINTED**\n\tx: %v\n\ty: %v",x,y)
}
