package numeric

import "math"
import "fmt"

func NewtonRaphson1D(f func(float64) float64, x0, dx,tol float64,maxiter int) (float64,error) {
	x := x0
	var dy float64
	var nx float64
	var y float64
	var err error
	found := false
	for iter := 0; !found && iter < maxiter; iter++ {
		y = f(x)
		dy = f(x+dx) - y
		if math.Abs(dy) < dx {
			return 0, fmt.Errorf("Small denominator")
		}

		nx = x - y * dx / dy

		if math.Abs(nx - x) < tol {
			found = true
		} else {
			x = nx
		}
	}

	if found {
		err = nil
	} else {
		err = fmt.Errorf("Failed to converge")
	}

	return nx, err

}
