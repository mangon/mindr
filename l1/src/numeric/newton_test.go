package numeric

import "testing"
import "math"

func f(x float64) float64 {
	return x*x - 2
}

func TestNR(t *testing.T) {
	x0 := 1.0
	dx := 0.001
	tol:= 0.01
	maxiter := 20
	xs := math.Sqrt(2)
	s,err := NewtonRaphson1D(f, x0, dx, tol, maxiter)

	if err != nil {
		t.Errorf("Process error: %v",err)
	} else if math.Abs(s - xs) > tol {
		t.Errorf("Failed solution: real (%f) != comp (%f)",xs,s) 
	}
}
