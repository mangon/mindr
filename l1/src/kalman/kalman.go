// Package kalman provides functions to compute basic Kalman Filter computations.
// In particular, it works for LTI models x' = A x + B u + w ; y = C x + v
// with gaussian noises w, v defined by covariance matrices Q,R (resp.)
//
// In addition, a function is provided to simulate multidimensional noise 
// from a covariance matrix.
package kalman

import . "linalg"
import "math/rand"

// Function UpdateState computes the next system state, x' = A x + B u.
func UpdateState(A,B,x,u Matrix) Matrix {
	Bu := B.Mul( u )
	return A.Mul( x ).Plus( Bu )
}

// Function UpdateCovar computes the next covariance, P' = APAt + Q.
func UpdateCovar(A,P,Q Matrix) Matrix {
	AT := A.Transpose()
	return A.Mul( P ).Mul( AT ).Plus( Q )
}

// Function CorrectState apply the state correction given observation z. 
func CorrectState(K, H, x, z Matrix) Matrix {
	A1 := H.Mul(x).Scale(-1)
	A2 := z.Plus( A1 )
	A3 := K.Mul( A2 )
	return x.Plus( A3 )
}

// Function CorrectCovar apply the covariance correction.
func CorrectCovar(K, H, P Matrix, n int) Matrix {
	A1 := K.Mul( H ).Scale( -1 )
	return Eye(n).Plus( A1 ).Mul( P )
}

// Function Gain computes the gain K = P Ht inverse(H P Ht + R)
func Gain(P, H, R Matrix) Matrix {
	PHt := P.Mul( H.Transpose() )
	S := (H.Mul( PHt).Plus(R)).Inverse()
	return PHt.Mul( S )
}


// Function Measure computes the observation of state x.
func Measure(H, x Matrix) Matrix {
	return H.Mul( x )
}

// Function CovarNoise, given a covariance matrix Sigma, returns a function that generates gaussian noise with the given covariance and mean 0.
func CovarNoise(Sigma Matrix) func() Matrix {
	w := Sigma.Cholesky()
	_,m := w.Dim()
	x := Zeros(m,1)
	return func() Matrix {
		for i := 0; i < m; i++ {
			x.SetAt(i,0, rand.NormFloat64() )
		}
		return w.Mul( x )
	}
}

// Function ProcessKalmanFilter applies the prediction and correction steps given an initial state, (x0,P0), a model (A,B,Q,H,R) and a sequence of observations and controls Z,U.
//
// The result is a sequence of state estimations.
func ProcessKalmanFilter(A, B, Q, H, R, x0, P0 Matrix, Z,U []Matrix) []Matrix {
	numSteps := len(Z)
	xhat := make([]Matrix,  numSteps)
	n,_ := A.Dim()
	//_,p := B.Dim()
	//m,_ := R.Dim()
	x := x0.Clone()
	P := P0.Clone()
	for k := 0; k < numSteps; k++ {
		u := U[k]
		x = UpdateState(A, B, x, u)
		P = UpdateCovar(A, P, Q)

		z := Z[k]
		K := Gain(P, H, R)
		x = CorrectState(K, H, x, z)
		P = CorrectCovar(K, H, P, n)

		xhat[k] = x.Clone()
	}

	return xhat
}
