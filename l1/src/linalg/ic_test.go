package linalg

import "testing"

func ictest(c, n, m int) bool {
	a := Matrix{n: n, m: m}
	i, j := a.Coord(c)
	d := a.Index(i, j)
	return c == d
}

func Benchmark_IC(b *testing.B) {
	for c := 0; c < b.N; c++ {
		ictest(c, 100, 100)
	}
}

func Test_IC_2(t *testing.T) {
	n, m := 100, 100
	a := Matrix{n: n, m: m}
	for i := 0; i < n; i++ {
		for j := 0; j < m; j++ {
			ij := a.Index(i, j)
			u, v := a.Coord(ij)
			if u != i || v != j {
				t.Errorf("n,m: %v,%v\ni,j,ij: %d,%d,%d\nu,v:%v,%v", n, m, i, j, ij, u, v)
			}
		}
	}
}

func Test_IC_100x100(t *testing.T) {

	nmax := 100
	mmax := 100
	for n := 0; n <= nmax; n++ {
		for m := 0; m <= mmax; m++ {
			for c := 0; c < n*m; c++ {
				if !ictest(c, n, m) {
					t.Errorf("n,m,coord: %d,%d,%d", n, m, c)
				}
			}
		}
	}
}
