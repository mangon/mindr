#fC Task: Mindstorms' SLAM

##Introduction
### Goal
Perform SLAM in a laboratory environment using a standard assembly of Mindstorms. Use the environment representation as a component of a higher-level AI beliefs.

### Environment
The environment has two types of entities: "obstacles" and "zones". While obstacles can be detected by the distance sonar and/or the touch sensor, zones can be detected by a light sensor pointing downward. A further refinement of the environment would separate "buildings" detected by the sonar from "barriers" detected by the touch sensor.

For simplicity we also assume that obstacles and zones have rectangular footsteps, aligned with some (unknown) referential.

Thus, a formal description of the environment, **to be used by a higher level AI** would be $$$x=(x^O,x^Z,x^P)$$$ where
$$
x^O = \left< (x,y,w,h)_i\ \middle|\ i \in 1,\ldots, N^O \right>
$$
describes obstacles
$$
x^Z = \left< (x,y,w,h)\ \middle|\ i \in 1,\ldots, N^Z \right>
$$
describes zones the robot pose is given by
$$
x^P = \left(x,y,\theta\right)
$$

Of course, varied shapes and orientations and also representing the robot with more than a point are goals in the horizon.

### Robot
The robot body is constructed following the basic body assembly instructions found in the LEGO Mindstorms Education set N. 9797. This vehicle is then augmented with fixed:

* forward facing distance sonar, 
* forward facing touch sensor,
* down facing light sensor.

The output of a sensor reading operation is a tuple
$$
z = \left(z^D , z^T , z^L , z^A , z^B\right)
$$
encoding the values of, respectively, _distance, touch, light, left wheel odometer, right wheel odometer_

The robot uses a differential drive motor control: it has a left and right motorized wheels that can be independently activated by sending rotation command
$$
u = \left(u^A,u^B\right)
$$
that turn the, respectively, left, right, wheel (in degrees).

The robot is remotely operated through a custom java class that uses the leJOS library to maintain a bidirectional bluetooth channel with the robot.

At the moment of this writing, the channel latency is about 840ms, being the sensor readings the most time expensive operations, by far. **This is a problem to be fixed.**

## Environment Reconstruction

### Problem Formulation
**Given**

* A history of sensor readings:
    $$
    Z = \left< \left(z^D , z^T , z^L , z^A , z^B\right)_{t}\ \middle|\ t \in 1, \ldots, T\right>
    $$

* A history of previous environment estimations
    $$\hat{X} = \left< \left(\hat{x}^O,\hat{x}^Z,\hat{x}^P\right)_t\ \middle|\ t \in 1, \ldots, T\right>$$
  
* The previous command
    $$
    u = \left(u^A,u^N\right)
    $$  
* and the current sensor reading:
    $$
    z = \left(z^D , z^T , z^L , z^A , z^B\right)_{T+1}
    $$

**Update** the environment state estimation by computing
$$
\left(\hat{x}^O,\hat{x}^Z,\hat{x}^P\right)_{T+1}
$$

